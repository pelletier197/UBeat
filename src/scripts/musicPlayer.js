/* eslint-disable no-underscore-dangle,no-empty */
export const ReadingMode = Object.freeze({ random: 'random', regular: 'regular', repeat: 'repeat' });
export const MUSIC_INFO = 'MUSIC_INFO';

export default class MusicPlayer {

  const;
  defaultSong = {
    artistId: 0,
    collectionId: 0,
    trackId: 0,
    artistName: '',
    collectionName: '',
    trackName: '',
    collectionCensoredName: '',
    trackCensoredName: '',
    artistViewUrl: '',
    collectionViewUrl: '',
    trackViewUrl: '',
    previewUrl: '',
    artworkUrl30: '',
    artworkUrl60: '',
    artworkUrl100: '/static/img/logo.png',
    collectionPrice: 0,
    trackPrice: 0,
    releaseDate: '',
    discCount: 0,
    discNumber: 0,
    trackCount: 0,
    trackNumber: 0,
    trackTimeMillis: 0,
    country: '',
    currency: '',
    primaryGenreName: '',
    _id: ''
  };

  constructor() {
    this.currentReadingMode = ReadingMode.regular;
    this.tracks = [];
    this.playQueue = [];
    this.previousSongs = [];
    this.currentSong = null;
    this.playing = false;
    this.audio = new Audio();
    this.time = 0;
    this.playInfinite = true;

    if (!localStorage.getItem(MUSIC_INFO)) {
      localStorage.setItem(MUSIC_INFO, JSON.stringify({}));
    }
  }

  play() {
    this.playing = true;

    if (this.audio.paused) {
      this.audio.play();
    } else {
      if (this.playQueue.length === 0) {
        this.playQueue = this.tracks.slice(0);
        this.organizePlayQueue();
      }
      this.next();
    }
  }

  playSong(song) {
    const index = this.tracks.indexOf(song);

    if (index !== -1) {
      this.playing = true;
      this.previousSongs = this.tracks.slice(0, index);

      const to = this.tracks.length;
      this.playQueue = this.tracks.slice(index, to);

      this.next();
      this.organizePlayQueue();
    }
  }

  pause() {
    if (!this.audio.paused) {
      this.playing = false;
      this.audio.pause();
    }
  }

  switchPlaying() {
    if (!this.audio.paused) {
      this.pause();
    } else {
      this.play();
    }
  }

  setTracks(tracks) {
    this.previousSongs = [];
    this.playQueue = [];
    this.playing = false;
    this.currentSong = null;
    this.tracks = tracks;

    if (!this.audio.paused) {
      this.audio.pause();
    }

    this.organizePlayQueue();
  }

  next() {
    this.organizeNext();
  }

  organizeNext() {
    if (this.playQueue.length === 0) {
      if (this.playInfinite) {
        this.playQueue = this.tracks.slice(0);
        this.organizePlayQueue();
      } else {
        this.playing = false;
      }
    }

    if (this.currentSong !== null) {
      this.previousSongs.push(this.currentSong);
    }

    this.playInternal(this.playQueue.shift());
  }

  previous() {
    if (this.audio.currentTime < 3) {
      if (this.previousSongs.length === 0) {
        this.previousSongs.push(this.playQueue.pop());
      }

      if (this.currentSong != null) {
        this.playQueue.unshift(this.currentSong);
      }

      this.playInternal(this.previousSongs.pop());
    } else {
      this.audio.currentTime = 0;
    }
  }

  togglePlayInfinite() {
    this.playInfinite = !this.playInfinite;
  }

  toggleReadingMode() {
    switch (this.currentReadingMode) {
      case ReadingMode.random:
        this.currentReadingMode = ReadingMode.repeat;
        break;
      case ReadingMode.repeat:
        this.currentReadingMode = ReadingMode.regular;
        break;
      case ReadingMode.regular:
        this.currentReadingMode = ReadingMode.random;
        break;
      default:
        break;
    }
    this.organizePlayQueue();
  }

  get currentPlaying() {
    return this.currentSong === null ? this.defaultSong : this.currentSong;
  }

  get readingMode() {
    return this.currentReadingMode;
  }

  get isPlayInfinite() {
    return this.playInfinite;
  }

  get isPlaying() {
    return this.playing;
  }

  get currentTime() {
    return this.time ? this.time : 0;
  }

  get totalDuration() {
    const duration = this.audio.duration;
    return Number.isNaN(duration) ? 0 : parseInt(duration, 10);
  }

  organizeRegular() {
    const currentPlayingIndex = this.tracks.indexOf(this.currentSong);

    if (currentPlayingIndex !== -1) {
      if (currentPlayingIndex === this.tracks.length - 1) {
        this.playQueue = this.tracks.slice(0);
      } else {
        // Organizes the playlist in order from where it is right now
        const to = this.tracks.length;
        this.playQueue = this.tracks.slice(currentPlayingIndex, to);
        this.playQueue.shift();
      }
    } else {
      // Takes the whole playlist as the play queue
      this.playQueue = this.tracks.slice(0);
    }
  }

  organizeRandom() {
    let j;
    let x;
    let i;

    for (i = this.playQueue.length - 1; i > 0; i -= 1) {
      j = Math.floor(Math.random() * (i + 1));
      x = this.playQueue[i];
      this.playQueue[i] = this.playQueue[j];
      this.playQueue[j] = x;
    }

    this.playQueue = this.playQueue.slice(0);
  }

  organizeRepeat() {
    this.playQueue = [];
    this.previousSongs = [];
    this.playQueue.push(this.currentPlaying);
  }

  organizePlayQueue() {
    switch (this.currentReadingMode) {
      case ReadingMode.random:
        this.organizeRandom();
        break;
      case ReadingMode.repeat:
        this.organizeRepeat();
        break;
      default:
        this.organizeRegular();
        break;
    }
  }

  playInternal(song) {
    if (song) {
      this.currentSong = song;
      this.audio.setAttribute('src', song.previewUrl);
      this.audio.addEventListener('ended', () => {
        this.next();
      });

      this.audio.ontimeupdate = () => {
        this.time = parseInt(this.audio.currentTime, 10);
      };

      this.audio.load();

      if (this.playing && this.audio.paused) {
        this.audio.play().catch(() => {});
      }

      MusicPlayer.saveSong(song);
    }
  }

  static saveSong(song) {
    const storage = JSON.parse(localStorage.getItem(MUSIC_INFO));
    const count = storage[song.primaryGenreName];

    if (!count) {
      storage[song.primaryGenreName] = 1;
    } else {
      storage[song.primaryGenreName] = count + 1;
    }
    localStorage.setItem(MUSIC_INFO, JSON.stringify(storage));
  }
}
