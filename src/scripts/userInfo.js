export default class UserInfo {
  constructor() {
    this.reset();
  }

  reset() {
    this.info = {
      name: '',
      email: '',
      token: null,
      id: null
    };
  }

  set userInfo(info) {
    this.info = info;
  }

  get userInfo() {
    return this.info;
  }
}
