import ApiHelper from './apiHelper';

export default class ArtistAPIHandler {
  static getArtist(artistId, handler) {
    ApiHelper.get(`/artists/${artistId}`, handler);
  }
  static getAlbumsForArtist(artistId, handler) {
    ApiHelper.get(`/artists/${artistId}/albums`, handler);
  }
  static getImgArtist(artistId, handler) {
    this.getAlbumsForArtist(artistId, (response) => {
      handler(response.data.results[0].artworkUrl100.replace('100x100', '400x400'));
    });
  }

}
