import Vue from 'vue';
import Router from 'vue-router';
import Toasted from 'vue-toasted';
import Album from '@/views/Album';
import Artist from '@/views/Artist';
import Playing from '@/components/Playing';
import PlayingQueue from '@/views/PlayingQueue';
import Settings from '@/views/Settings';
import Login from '@/views/Login';
import Home from '@/views/Home';
import Playlists from '@/views/Playlists';
import UserInfo from '@/scripts/userInfo';
import VueScrollTo from 'vue-scrollto';
import AudioVisual from 'vue-audio-visual';
import MusicPlayer, { ReadingMode } from '@/scripts/musicPlayer';
import Profile from '@/views/Profile';

Vue.use(Router);
Vue.use(VueScrollTo);
Vue.use(AudioVisual);
Vue.use(Toasted, {
  position: 'top-right',
  duration: 2000
});

Vue.prototype.$player = new MusicPlayer();
Vue.prototype.$ReadingMode = ReadingMode;
Vue.prototype.$userInfo = new UserInfo();

export default new Router({
  routes: [
    {
      path: '/profile/:userId',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/artist/:artistId',
      name: 'Artist',
      component: Artist,
      canReuse: false, // force to reload data
    }, {
      path: '/album/:albumId',
      name: 'Album',
      component: Album,
      canReuse: false, // force to reload data
    }, {
      path: '/playing',
      name: 'Playing',
      component: Playing
    }, {
      path: '/playing-queue',
      name: 'Playing Queue',
      component: PlayingQueue
    }, {
      path: '/playlists',
      name: 'Playlists',
      component: Playlists
    }, {
      path: '/login',
      name: 'Login',
      component: Login
    }, {
      path: '/settings',
      name: 'Settings',
      component: Settings
    }, {
      path: '/',
      name: 'Home',
      component: Home
    }
  ],
});
