import ApiHelper from './apiHelper';

export default class AlbumAPIHandler {
  static getAlbum(albumId, handler) {
    ApiHelper.get(`/albums/${albumId}`, handler);
  }
  static getAlbumTracks(albumId, handler) {
    ApiHelper.get(`/albums/${albumId}/tracks`, handler);
  }
}
