import ApiHelper from './apiHelper';

export default class UserApiHandler {
  static getUser(userId, handler) {
    ApiHelper.get(`/users/${userId}`, handler);
  }

  static followUser(user, handler) {
    ApiHelper.post('/follow', handler, user);
  }

  static unfollowUser(userId, handler) {
    ApiHelper.delete(`/follow/${userId}`, handler);
  }
}
