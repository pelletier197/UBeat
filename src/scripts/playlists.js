/* eslint-disable no-unneeded-ternary */
import Vue from 'vue';
import ApiHelper, { owner } from './apiHelper';

export default class PlaylistAPIHandler {

  static addTrackToPlaylist(playlistId, track, handler) {
    ApiHelper.post(`/playlists/${playlistId}/tracks`, handler, track);
  }

  static deleteTrackFromPlaylist(playlistId, trackId, handler) {
    ApiHelper.delete(`/playlists/${playlistId}/tracks/${trackId}`, handler);
  }

  static getPlaylists(handler, id) {
    ApiHelper.get('/playlists', (response) => {
      const uid = id ? id : Vue.prototype.$userInfo.userInfo.id;
      response.data = response.data.filter(x => x.owner && x.owner.id === uid);
      handler(response);
    });
  }

  static deletePlaylist(playlistId, handler) {
    ApiHelper.delete(`/playlists/${playlistId}`, handler);
  }

  static editPlaylist(playlist, name, handler) {
    ApiHelper.put(`/playlists/${playlist.id}`, handler, playlist);
  }

  static createPlaylist(name, handler) {
    if (name) {
      ApiHelper.post('/playlists', handler, {
        name,
        owner
      });

      return true;
    }

    return false;
  }
}
