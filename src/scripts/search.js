import ApiHelper from './apiHelper';

export default class SearchAPIHandler {
  static getSearch(field, handler) {
    ApiHelper.get(`/search/?q=${encodeURIComponent(field)}`, (response) => {
      SearchAPIHandler.getUsersResearch(field, (userResponse) => {
        response.data.results = response.data.results.concat(userResponse.data);
        handler(response);
      });
    });
  }

  static getArtistsResearch(artistName, handler) {
    ApiHelper.get(`/search/artists/?q=${encodeURIComponent(artistName)}`, handler);
  }

  static getAlbumsResearch(albumName, handler) {
    ApiHelper.get(`/search/albums/?q=${encodeURIComponent(albumName)}`, handler);
  }

  static getTracksResearch(trackName, handler) {
    ApiHelper.get(`/search/tracks/?q=${encodeURIComponent(trackName)}`, handler);
  }

  static getUsersResearch(userName, handler) {
    ApiHelper.get(`/search/users/?q=${encodeURIComponent(userName)}`, handler);
  }
}
