import axios from 'axios/index';
import Vue from 'vue';

const baseRoute = 'http://ubeat.herokuapp.com';

const routesAvailableWithoutToken = ['/login', '/signup'];
let isTokenValid = true;

function params(type, route, data) {
  const token = localStorage.getItem('token');

  return {
    method: type,
    url: `${baseRoute}${route}`,
    data: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
      Authorization: token
    },
    json: true
  };
}

function ensureCanAccessRoute(route) {
  if (routesAvailableWithoutToken.includes(route)) {
    return true;
  }

  const token = localStorage.getItem('token');

  if (!token) {
    Vue.prototype.$globalRouter.push('/login');
    isTokenValid = false;
    return false;
  }

  if (Vue.prototype.$userInfo) {
    if (!Vue.prototype.$userInfo.userInfo.token) {
      const storage = localStorage.getItem('userInfo');

      if (storage) {
        Vue.prototype.$userInfo.userInfo = JSON.parse(storage);
      }

      axios(params('GET', '/tokeninfo'))
        .then((response) => {
          Vue.prototype.$userInfo.userInfo = response.data;
          localStorage.setItem('userInfo', JSON.stringify(response.data));
          isTokenValid = true;
        }).catch((error) => {
          if (error.response.status === 401) {
            isTokenValid = false;
          }
        });
    }
  }

  if (!isTokenValid) {
    Vue.prototype.$globalRouter.push('/login');
    return false;
  }

  return true;
}

export default class ApiHelper {

  static post(route, handler, data, error) {
    if (ensureCanAccessRoute(route)) {
      axios(params('POST', route, data))
        .then((response) => {
          isTokenValid = true;
          if (handler) {
            handler(response);
          }
        }).catch((xpt) => {
          if (xpt.response.status === 401) {
            isTokenValid = false;
            Vue.prototype.$globalRouter.push('/login');
          } else if (error) {
            error(xpt);
          } else {
            Vue.toasted.error('Communication error with server occurred');
          }
        });
    }
  }

  static get(route, handler, error) {
    if (ensureCanAccessRoute(route)) {
      axios(params('GET', route))
        .then((response) => {
          isTokenValid = true;
          if (handler) {
            handler(response);
          }
        }).catch((xpt) => {
          if (xpt.response.status === 401) {
            isTokenValid = false;
            Vue.prototype.$globalRouter.push('/login');
          } else if (error) {
            error(xpt);
          } else {
            Vue.toasted.error('Communication error with server occurred');
          }
        });
    }
  }

  static put(route, handler, data, error) {
    if (ensureCanAccessRoute(route)) {
      axios(params('PUT', route, data))
        .then((response) => {
          isTokenValid = true;
          if (handler) {
            handler(response);
          }
        }).catch((xpt) => {
          if (xpt.response.status === 401) {
            isTokenValid = false;
            Vue.prototype.$globalRouter.push('/login');
          } else if (error) {
            error(xpt);
          } else {
            Vue.toasted.error('Communication error with server occurred');
          }
        });
    }
  }

  static delete(route, handler, error) {
    if (ensureCanAccessRoute(route)) {
      axios(params('DELETE', route))
        .then((response) => {
          isTokenValid = true;

          if (handler) {
            handler(response);
          }
        }).catch((xpt) => {
          if (xpt.response.status === 401) {
            isTokenValid = false;
            Vue.prototype.$globalRouter.push('/login');
          } else if (error) {
            error(xpt);
          } else {
            Vue.toasted.error('Communication error with server occurred');
          }
        });
    }
  }
}
