import Vue from 'vue';
import ApiHelper from './apiHelper';

export default class LoginAPIHandler {
  static signUp(firstname, lastname, email, password, handler, error) {
    ApiHelper.post('/signup', handler, {
      name: `${firstname} ${lastname}`,
      email,
      password
    }, error);
  }

  static logIn(email, password, handler, error) {
    ApiHelper.post('/login', (response) => {
      localStorage.setItem('token', response.data.token);
      handler(response);
    }, {
      email,
      password
    }, error);
  }

  static logOut(handler) {
    ApiHelper.get('/logout', (response) => {
      localStorage.setItem('token', '');
      Vue.prototype.$userInfo.reset();
      Vue.prototype.$player.pause();
      handler(response);
    });
  }
}
