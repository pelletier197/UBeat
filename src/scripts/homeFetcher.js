/* eslint-disable guard-for-in,no-restricted-syntax,no-loop-func */
import ApiHelper from './apiHelper';
import { MUSIC_INFO } from './musicPlayer';

// Based on http://amorphousblog.com/2010/most-common-words-in-album-names-band-names-and-song-titles/
const searchValues = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
  'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'the', 'and', 'of', 'ost', 'to', 'love', 'in', 'me', 'my'];


export default class homeAPIHandler {

  constructor() {
    let songs = [];
    this.songs = [];
    for (let i = 0; i < 8; i += 1) {
      const letter = searchValues[Math.floor(Math.random() * searchValues.length)];
      ApiHelper.get(`/search/albums?q=${letter}&limit=1000`, ((data) => {
        songs = songs.concat(data.data.results);
      }));
    }

    setTimeout(() => {
      this.songs = songs;
    }, 2000);
  }

  getRandomGenre() {
    if (this.songs.length < 16) {
      return { genre: '', songs: [] };
    }

    const favorite = homeAPIHandler.getFavoriteGenres();
    const chosen = { genre: '' };

    do {
      const index = Math.floor(Math.random() * this.songs.length);
      chosen.genre = this.songs[index].primaryGenreName;
    } while (chosen.genre === favorite[0].genre || chosen.genre === favorite[1].genre);

    const chosenSongs = this.getFavorites(chosen);
    return { genre: chosen.genre, songs: homeAPIHandler.chooseRandom(chosenSongs, 16) };
  }

  getRandomAlbums() {
    return { genre: null, songs: homeAPIHandler.chooseRandom(this.songs, 16) };
  }

  getRandomAlbumsForFavoriteGenre() {
    const genre = homeAPIHandler.getFavoriteGenres()[0];
    const favorite = this.getFavorites(genre);
    if (favorite) {
      return { genre: genre.genre, songs: homeAPIHandler.chooseRandom(favorite, 16) };
    }
    return { genre: null, songs: [] };
  }

  static chooseRandom(array, count) {
    if (array.length < count) {
      return [];
    }
    const random = [];

    for (let i = 0; i < count; i += 1) {
      const index = Math.floor(Math.random() * array.length);
      random.push(array[index]);
      array.splice(index, 1);
    }

    return random;
  }

  getFavorites(genre) {
    const result = [];
    for (const s in this.songs) {
      if (this.songs[s].primaryGenreName === genre.genre) {
        result.push(this.songs[s]);
      }
    }
    return result;
  }

  getRandomAlbumsForSecondFavoriteGenre() {
    const genre = homeAPIHandler.getFavoriteGenres()[1];
    const secondFavorite = this.getFavorites(genre);

    if (secondFavorite) {
      return { genre: genre.genre, songs: homeAPIHandler.chooseRandom(secondFavorite, 16) };
    }
    return { genre: null, songs: [] };
  }

  static getFavoriteGenres() {
    const storage = JSON.parse(localStorage.getItem(MUSIC_INFO));

    let maxMin = { genre: '', count: 0 };
    let maxMax = { genre: '', count: 0 };

    for (const k in storage) {
      let keep = true;
      if (storage[k] > maxMax.count) {
        maxMin = maxMax;
        maxMax = { genre: k, count: storage[k] };
        keep = false;
      }

      if (keep && storage[k] > maxMin.count) {
        maxMin = { genre: k, count: storage[k] };
      }
    }

    return [maxMax, maxMin];
  }
}
